export default function (fn: Function, delay?: number, immediate?: boolean): () => void;
