type StorageUnit = "B" | "KB" | "MB" | "GB" | "TB";
export default function (value: number, unit?: StorageUnit): {
    convert(unit: StorageUnit, decimalCount?: number): number;
    auto(decimalCount?: number): string;
};
export {};
