declare const _default: {
    number: (min: number, max: number) => number;
    id(): string;
    english(count?: number, type?: "smallLetter" | "capitalization" | "mix"): string;
    chinese(count?: number): string;
};
export default _default;
