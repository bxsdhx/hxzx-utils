type Func = (...parameter: any[]) => any;
declare const _default: {
    event: Map<string, Func[]>;
    emit(id: string, ...parameter: any[]): void;
    on(id: string, func: Func): void;
    off(id?: string, func?: Func): void;
    getSize(id?: string): number;
};
export default _default;
