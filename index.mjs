var y = Object.defineProperty;
var B = (t, e, r) => e in t ? y(t, e, { enumerable: !0, configurable: !0, writable: !0, value: r }) : t[e] = r;
var m = (t, e, r) => (B(t, typeof e != "symbol" ? e + "" : e, r), r);
const N = new class {
  constructor() {
    /**
     * 事件
     */
    m(this, "event", /* @__PURE__ */ new Map());
  }
  /**
   * @description: 通知
   * @param {string} id ID
   * @param {...array} parameter 参数
   * @return {void}
   */
  emit(e, ...r) {
    const n = this.event.get(e);
    n && n.forEach((s) => s(...r));
  }
  /**
   * @description: 监听
   * @param {string} id ID
   * @param {Func} func 回调
   * @return {void}
   */
  on(e, r) {
    var n;
    this.event.has(e) || this.event.set(e, []), (n = this.event.get(e)) == null || n.push(r);
  }
  /**
   * @description: 取消监听
   * @param {string} id ID
   * @param {Func} func 回调
   *
   * 取消所有的订阅 都不传递
   *
   * 针对名称下所有的函数取消订阅 只传递id
   *
   * 针对名称和函数取消订阅 传递id和func
   *
   * @return {void}
   */
  off(e, r) {
    if (!e && !r) {
      this.event.clear();
      return;
    }
    if (!e)
      return;
    const n = this.event.get(e);
    if (!n)
      return;
    if (!r) {
      this.event.delete(e);
      return;
    }
    const s = [];
    for (let i = 0; i < n.length; i++)
      n[i].toString() !== r.toString() && s.push(n[i]);
    s.length > 0 ? this.event.set(e, s) : this.event.delete(e);
  }
  /**
   * @description: 查询已订阅的名称数量
   * @param {string} id ID
   *
   * 查询已订阅的名称数量 不传递id
   *
   * 查询某个名称下的订阅数量 传递id
   *
   * @return {number}
   */
  getSize(e) {
    if (typeof e != "string")
      return this.event.size;
    const r = this.event.get(e);
    return r ? r.length : 0;
  }
}();
class S {
  constructor() {
    /**
     * 队列列表
     */
    m(this, "task", []);
    /**
     * 执行状态
     */
    m(this, "status", !1);
  }
  /**
   * 加入新的任务
   */
  push(e) {
    this.task.push(e), this.execute();
  }
  /**
   * 获取当前任务数量
   */
  getSize() {
    return this.task.length;
  }
  /**
   * 执行下一个任务
   */
  execute() {
    this.task.length <= 0 || this.status || (this.status = !0, this.task[0](() => {
      this.status = !1, this.task.splice(0, 1), this.execute();
    }));
  }
}
function T(t, e = 200, r = !1) {
  let n;
  return function() {
    n || (r ? (t.apply(null, arguments), n = globalThis.setTimeout(() => n = void 0, e)) : n = globalThis.setTimeout(() => {
      t.apply(null, arguments), n = void 0;
    }, e));
  };
}
function L(t, e = 200, r = !1) {
  let n;
  return function() {
    n || (r && t.apply(null, arguments), n = globalThis.setTimeout(() => {
      r || t.apply(null, arguments), n = void 0;
    }, e));
  };
}
async function w(t = 0) {
  if (!(typeof t != "number" || t <= 0))
    return new Promise(function(e) {
      setTimeout(e, t);
    });
}
const o = function(t) {
  const r = Object.prototype.toString.call(t).replace(/\[|]|object|\s/g, "").toLocaleLowerCase();
  return r !== "number" ? r : isNaN(t) ? "nan" : r;
}, f = {
  /**
   * @description: 获取某个数据类型
   * @param {any} v 数据
   * @return {DataType} 数据类型
   */
  getDataType: o,
  /**
   * @description: 判断数据类型是否是 string
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isString(t) {
    return o(t) === "string";
  },
  /**
   * @description: 判断数据类型是否是 number
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isNumber(t) {
    return o(t) === "number";
  },
  /**
   * @description: 判断数据类型是否是 bigint
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isBigint(t) {
    return o(t) === "bigint";
  },
  /**
   * @description: 判断数据类型是否是 array
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isArray(t) {
    return o(t) === "array";
  },
  /**
   * @description: 判断数据类型是否是 object
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isObject(t) {
    return o(t) === "object";
  },
  /**
   * @description: 判断数据类型是否是 symbol
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isSymbol(t) {
    return o(t) === "symbol";
  },
  /**
   * @description: 判断数据类型是否是 function
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isFunction(t) {
    return o(t) === "function";
  },
  /**
   * @description: 判断数据类型是否是 boolean
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isBoolean(t) {
    return o(t) === "boolean";
  },
  /**
   * @description: 判断数据类型是否是 null
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isNull(t) {
    return o(t) === "null";
  },
  /**
   * @description: 判断数据类型是否是 undefined
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isUndefined(t) {
    return o(t) === "undefined";
  },
  /**
   * @description: 判断数据类型是否是 nan
   * @param {any} v 数据
   * @return {boolean} 是否符合
   */
  isNan(t) {
    return o(t) === "nan";
  }
};
function M(t, e = "up") {
  t += 1;
  let r = "";
  for (; t > 0; ) {
    let n = t % 26;
    n = n === 0 ? n = 26 : n, r = String.fromCharCode(96 + parseInt(n)) + r, t = (t - n) / 26;
  }
  return e === "down" ? r : r.toLocaleUpperCase();
}
const p = function(t, e) {
  const r = Math.min(t, e), s = Math.max(t, e) - r;
  return s === 0 ? r : Math.floor(Math.random() * (s + 1)) + r;
}, O = {
  number: p,
  /**
   * @description: 生成随机ID
   * @return {string}
   */
  id() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function(t) {
        let e = Math.random() * 16 | 0;
        return (t === "x" ? e : e & 3 | 8).toString(16);
      }
    );
  },
  /**
   * @description: 生成随机字母
   * @param {number} count 字母长度 默认5 非必填
   * @param {"smallLetter" | "capitalization" | "mix"} type 字母类型 默认smallLetter 非必填
   * 
   * smallLetter 小写
   * 
   * capitalization 大写
   * 
   * mix 混合
   * 
   * @return {string}
   */
  english(t = 5, e = "smallLetter") {
    return Array.from({ length: t }).map(function() {
      const r = M(Math.floor(Math.random() * 26));
      return e === "capitalization" || e === "mix" && p(1, 2) === 1 ? r : r.toLocaleLowerCase();
    }).join("");
  },
  /**
   * @description: 生成随机中文
   * @param {number} count 字母长度 默认5 非必填
   * @return {string}
   */
  chinese(t = 5) {
    const e = [];
    for (let r = 0; r < t; r++) {
      let n = "";
      n = "\\u" + (Math.floor(Math.random() * (40869 - 19968)) + 19968).toString(16), n = unescape(n.replace(/\\u/g, "%u")), e.push(n);
    }
    return e.join("");
  }
};
function v(t) {
  t = t.toLocaleUpperCase();
  let e = "ABCDEFGHIJKLMNOPQRSTUVWXYZ", r = 0;
  for (let n = 0, s = t.length - 1; n < t.length; n += 1, s -= 1)
    r += Math.pow(e.length, s) * (e.indexOf(t[n]) + 1);
  return Math.max(0, r - 1);
}
const a = ["B", "KB", "MB", "GB", "TB"];
function j(t, e = "B") {
  let r = 0;
  const n = a.indexOf(e) === -1 ? a[0] : e;
  f.isNumber(t) && t > 0 && (r = t);
  function s(i, u = 0) {
    return u = Math.floor(u), !f.isNumber(u) || !u || !(u > 0) ? Math.floor(i) : Number(i.toFixed(u));
  }
  return {
    /**
     * @description: 转换
     * @param {StorageUnit} unit 转换后的单位
     * @param {number} decimalCount 保留几位小数 默认0 非必填
     * @return {number}
     */
    convert(i, u = 0) {
      if (r <= 0)
        return 0;
      const g = a.indexOf(i) === -1 ? a[0] : i, l = a.indexOf(n), h = a.indexOf(g);
      if (l === -1 || h === -1)
        return 0;
      let c = 0;
      if (l === h)
        c = t;
      else if (l < h) {
        const b = h - l;
        for (let x = 0; x < b; x++)
          t /= 1024;
        c = t;
      } else if (l > h) {
        const b = l - h;
        for (let x = 0; x < b; x++)
          t *= 1024;
        c = t;
      }
      return u = Math.floor(u), !f.isNumber(u) || !u || !(u > 0) ? Math.floor(c) : Number(c.toFixed(u));
    },
    /**
     * @description: 自动转换
     * @param {number} decimalCount 保留几位小数 默认0 非必填
     * @return {string} 转后的值，保留存储单位
     */
    auto(i = 0) {
      if (r < 1024)
        return `${r}${n}`;
      const u = a.indexOf(n);
      for (let g = u; g > 0; g--)
        r *= 1024;
      return r < 1024 ? `${s(Math.max(0, r), i)}B` : r < 1024 * 1024 ? `${s(r / 1024, i)}KB` : r < 1024 * 1024 * 1024 ? `${s(r / 1024 / 1024, i)}MB` : r < 1024 * 1024 * 1024 * 1024 ? `${s(r / 1024 / 1024 / 1024, i)}GB` : `${s(r / 1024 / 1024 / 1024 / 1024, i)}TB`;
    }
  };
}
function I(t, e) {
  return !f.isString(t) || !t ? "" : (e = Math.max(
    -1,
    f.isNumber(e) ? Math.floor(e) : -1
  ), e <= 0 ? t : t.length > e ? t.substring(0, e) + "..." : t);
}
const d = {
  /**
   * @description: 校验内容是否是字符串，并且不为空，你也可以自定义字符串的长度范围
   * @param {any} value 内容
   * @param {number} options.min 非必传，默认1，传递时需保证大于等于1，不符合按1处理
   * @param {number} options.max 非必传，大于等于options.min即可
   * @return {*}
   */
  string(t, e) {
    if (!f.isString(t) || t === "")
      return !1;
    f.isObject(e) || (e = {});
    const r = Math.max(1, (e == null ? void 0 : e.min) || 1);
    return t.length < r ? !1 : e && e.max && f.isNumber(e.max) && e.max > r ? !(t.length > e.max) : !0;
  }
}, U = {
  version: "1.0.4",
  eventBus: N,
  queue: S,
  throttle: T,
  debounce: L,
  wait: w,
  type: f,
  random: O,
  indexToLetter: M,
  letterToIndex: v,
  storageConversion: j,
  stringEllipsis: I,
  verification: d
};
export {
  U as default
};
