export default class queue {
    private task;
    private status;
    push(callback: (done: () => void) => void): void;
    getSize(): number;
    private execute;
}
