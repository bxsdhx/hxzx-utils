declare const _default: {
    string(value: any, options?: {
        min?: number;
        max?: number;
    }): boolean;
};
export default _default;
