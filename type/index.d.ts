type DataType = "string" | "number" | "bigint" | "array" | "object" | "symbol" | "function" | "boolean" | "null" | "undefined" | "nan";
declare const _default: {
    getDataType: (v?: any) => DataType;
    isString(v?: any): boolean;
    isNumber(v?: any): boolean;
    isBigint(v?: any): boolean;
    isArray(v?: any): boolean;
    isObject(v?: any): boolean;
    isSymbol(v?: any): boolean;
    isFunction(v?: any): boolean;
    isBoolean(v?: any): boolean;
    isNull(v?: any): boolean;
    isUndefined(v?: any): boolean;
    isNan(v?: any): boolean;
};
export default _default;
