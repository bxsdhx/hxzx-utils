import queue from './queue';
import throttle from './throttle';
import debounce from './debounce';
import wait from './wait';
import indexToLetter from './indexToLetter';
import letterToIndex from './letterToIndex';
import storageConversion from './storageConversion';
import stringEllipsis from './stringEllipsis';
declare const hxzxUtils: {
    version: string;
    eventBus: {
        event: Map<string, ((...parameter: any[]) => any)[]>;
        emit(id: string, ...parameter: any[]): void;
        on(id: string, func: (...parameter: any[]) => any): void;
        off(id?: string | undefined, func?: ((...parameter: any[]) => any) | undefined): void;
        getSize(id?: string | undefined): number;
    };
    queue: typeof queue;
    throttle: typeof throttle;
    debounce: typeof debounce;
    wait: typeof wait;
    type: {
        getDataType: (v?: any) => "string" | "number" | "bigint" | "boolean" | "symbol" | "undefined" | "object" | "function" | "array" | "null" | "nan";
        isString(v?: any): boolean;
        isNumber(v?: any): boolean;
        isBigint(v?: any): boolean;
        isArray(v?: any): boolean;
        isObject(v?: any): boolean;
        isSymbol(v?: any): boolean;
        isFunction(v?: any): boolean;
        isBoolean(v?: any): boolean;
        isNull(v?: any): boolean;
        isUndefined(v?: any): boolean;
        isNan(v?: any): boolean;
    };
    random: {
        number: (min: number, max: number) => number;
        id(): string;
        english(count?: number, type?: "smallLetter" | "capitalization" | "mix"): string;
        chinese(count?: number): string;
    };
    indexToLetter: typeof indexToLetter;
    letterToIndex: typeof letterToIndex;
    storageConversion: typeof storageConversion;
    stringEllipsis: typeof stringEllipsis;
    verification: {
        string(value: any, options?: {
            min?: number | undefined;
            max?: number | undefined;
        } | undefined): boolean;
    };
};
export default hxzxUtils;
