# 介绍

### 基于 javascript 封装的工具库

[文档地址](https://jjp.world/hxzx-utils)

该项目属于个人开发，暂未开源，若后期稳定，将考虑开源至 [码云](https://gitee.com/)

### 兼容性

| node | 浏览器 | 小程序 |
| ---- | ------ | ------ |
| 支持 | 支持   | 支持   |

### 版本

[更新日志](https://jjp.world/hxzx-utils/#/version)

### 问题或建议

[反馈地址](https://gitee.com/bxsdhx/hxzx-utils/issues)
